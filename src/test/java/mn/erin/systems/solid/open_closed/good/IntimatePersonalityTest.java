package mn.erin.systems.solid.open_closed.good;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public class IntimatePersonalityTest
{
    @Test
    public void testGreetsSomeoneIntimately()
    {
        IntimatePersonality ip = new IntimatePersonality();
        assertEquals("Hello Darling!", ip.greet());
    }
}
