package mn.erin.systems.solid.open_closed.good;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public class FormalPersonalityTest {
    @Test
    public void testGreetsSomeoneFormally()
    {
        FormalPersonality fp = new FormalPersonality();
        assertEquals("Good evening, sir.", fp.greet());
    }
}
