package mn.erin.systems.solid.open_closed.good;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public class CasualPersonalityTest
{
    @Test
    public void testGreetsSomeoneCasually()
    {
        CasualPersonality cp = new CasualPersonality();
        assertEquals("Sup bro?", cp.greet());
    }
}
