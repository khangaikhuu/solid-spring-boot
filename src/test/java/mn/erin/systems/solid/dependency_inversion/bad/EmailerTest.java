package mn.erin.systems.solid.dependency_inversion.bad;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public class EmailerTest
{
    @Test
    public void testGeneratesAlertString()
    {
        Emailer emailer = new Emailer();
        assertEquals("It is sunny", emailer.generateWeatherAlert("sunny"));
    }
}
