package mn.erin.systems.solid.dependency_inversion.bad;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public class PhoneTest
{
    @Test
    public void testGeneratesAlertString()
    {
        Phone phone = new Phone();
        assertEquals("It is rainy", phone.generateWeatherAlert("rainy"));
    }
}
