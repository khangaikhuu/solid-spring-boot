package mn.erin.systems.solid.liskov_substitution.bad;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public class PenthouseSuiteTest {

    @Test
    public void testInitializedWithFourBedrooms() {
        PenthouseSuite penthouse = new PenthouseSuite();
        assertEquals(4, penthouse.numberOfBedrooms);
    }

    @Test
    public void testSetsSquareFootage() {
        PenthouseSuite penthouse = new PenthouseSuite();
        penthouse.setSquareFootage(1500);
        assertEquals(1500, penthouse.squareFootage);
    }
}
