package mn.erin.systems.solid;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SolidApplicationTests {

	@Test
	public void contextLoads() {
	}

}
