package mn.erin.systems.solid.interface_segregation.good;

import mn.erin.systems.solid.interface_segregation.bad.Penguin;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public class PenguinTest
{
    @Test
    public void testItCanSwim()
    {
        Penguin penguin = new Penguin(5);
        penguin.swim();
        assertEquals("in the water", penguin.currentLocation);
    }

    @Test
    public void testItLosesFeathersQuickly()
    {
        Penguin penguin = new Penguin(5);
        penguin.molt();
        assertEquals(1, penguin.numberOfFeathers);
    }
}
