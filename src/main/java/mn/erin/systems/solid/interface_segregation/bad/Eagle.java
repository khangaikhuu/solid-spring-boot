package mn.erin.systems.solid.interface_segregation.bad;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public class Eagle implements Bird
{
    String currentLocation;
    int numberOfFeathers;

    public Eagle(int initialFeatherCount)
    {
        this.numberOfFeathers = initialFeatherCount;
    }

    @Override
    public void fly()
    {
        this.currentLocation = "in the air";
    }

    @Override
    public void molt()
    {
        this.numberOfFeathers -= 1;
    }
}
