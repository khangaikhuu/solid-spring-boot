package mn.erin.systems.solid.interface_segregation.good;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public interface SwimmingCreature
{
    void swim();
}
