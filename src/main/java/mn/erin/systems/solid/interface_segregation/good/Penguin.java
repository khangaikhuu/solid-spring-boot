package mn.erin.systems.solid.interface_segregation.good;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public class Penguin implements SwimmingCreature, FeatheredCreature
{
    String currentLocation;
    int numberOfFeathers;

    public Penguin(int initialFeatherCount)
    {
        this.numberOfFeathers = initialFeatherCount;
    }

    public void swim()
    {
        this.currentLocation = "in the water";
    }

    public void molt()
    {
        this.numberOfFeathers -= 4;
    }
}
