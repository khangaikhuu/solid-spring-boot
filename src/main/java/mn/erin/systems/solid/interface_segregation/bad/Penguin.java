package mn.erin.systems.solid.interface_segregation.bad;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public class Penguin implements Bird
{
    public String currentLocation;
    public int numberOfFeathers;

    public Penguin(int initialFeatherCount)
    {
        this.numberOfFeathers = initialFeatherCount;
    }

    @Override
    public void molt()
    {
        this.numberOfFeathers -= 1;
    }

    @Override
    public void fly()
    {
        throw new UnsupportedOperationException();
    }

    public void swim()
    {
        this.currentLocation = "in the water";
    }
}
