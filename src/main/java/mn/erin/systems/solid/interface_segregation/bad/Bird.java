package mn.erin.systems.solid.interface_segregation.bad;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public interface Bird
{
    void fly();
    void molt();
}
