package mn.erin.systems.solid.open_closed.bad;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public class Greeter
{
    String formality;

    public String greet() {
        if (this.formality == "formal")
        {
            return "Good evening, sir.";
        }
        else if (this.formality == "casual")
        {
            return "Sup bro?";
        }
        else if (this.formality == "intimate")
        {
            return "Hello Darling!";
        }
        else
            {
            return "Hello.";
        }
    }

    public void setFormality(String formality)
    {
        this.formality = formality;
    }
}
