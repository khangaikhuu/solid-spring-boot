package mn.erin.systems.solid.open_closed.good;

public interface Personality
{
    String greet();
}
