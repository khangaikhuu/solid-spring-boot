package mn.erin.systems.solid.open_closed.good;

public class FormalPersonality implements Personality
{
    public String greet()
    {
        return "Good evening, sir.";
    }
}
