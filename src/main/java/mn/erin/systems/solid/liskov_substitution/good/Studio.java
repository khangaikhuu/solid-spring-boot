package mn.erin.systems.solid.liskov_substitution.good;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public class Studio
{
    int squareFootage;
    int numberOfRooms;

    public Studio()
    {
        this.numberOfRooms = 0;
    }

    public void setSquareFootage(int sqft)
    {
        this.squareFootage = sqft;
    }
}
