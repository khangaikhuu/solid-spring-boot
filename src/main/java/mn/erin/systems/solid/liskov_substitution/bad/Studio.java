package mn.erin.systems.solid.liskov_substitution.bad;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public class Studio extends Apartment
{
    public Studio()
    {
        this.numberOfBedrooms = 0;
    }

    @Override
    public void setSquareFootage(int sqft)
    {
        this.squareFootage = sqft;
    }
}
