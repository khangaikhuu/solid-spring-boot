package mn.erin.systems.solid.liskov_substitution.bad;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public class PenthouseSuite extends Apartment
{
    public PenthouseSuite()
    {
        this.numberOfBedrooms = 4;
    }

    @Override
    public void setSquareFootage(int sqft)
    {
        this.squareFootage = sqft;
    }
}
