package mn.erin.systems.solid.liskov_substitution.good;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public class BedroomAdder
{
    public void addBedroom(PenthouseSuite penthouse)
    {
        penthouse.numberOfBedrooms += 1;
    }
}
