package mn.erin.systems.solid.liskov_substitution.good;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public class PenthouseSuite
{
    int squareFootage;
    int numberOfBedrooms;

    public PenthouseSuite()
    {
        this.numberOfBedrooms = 4;
    }

    public void setSquareFootage(int sqft)
    {
        this.squareFootage = sqft;
    }
}
