package mn.erin.systems.solid.liskov_substitution.bad;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public class UnitUpgrader
{
    public void upgrade(Apartment apartment)
    {
        apartment.squareFootage += 40;

        if (apartment instanceof Studio)
        {
            apartment.numberOfBedrooms += 1;
        }
    }
}
