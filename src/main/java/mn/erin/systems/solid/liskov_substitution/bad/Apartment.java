package mn.erin.systems.solid.liskov_substitution.bad;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public abstract class Apartment
{
    int squareFootage;
    int numberOfBedrooms;

    public abstract void setSquareFootage(int sqft);
}
