package mn.erin.systems.solid.dependency_inversion.good;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public class WeatherTracker
{
    public String currentConditions;

    public void setCurrentConditions(String weatherDescription)
    {
        this.currentConditions = weatherDescription;
    }

    public void notify(Notifier notifier)
    {
        notifier.alertWeatherConditions(currentConditions);
    }
}
