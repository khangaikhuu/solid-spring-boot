package mn.erin.systems.solid.dependency_inversion.bad;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public class WeatherTracker
{
    String currentConditions;
    Phone phone;
    Emailer emailer;

    public WeatherTracker()
    {
        phone = new Phone();
        emailer = new Emailer();
    }

    public void setCurrentConditions(String weatherDescription)
    {
        this.currentConditions = weatherDescription;
        if (weatherDescription == "rainy")
        {
            String alert = phone.generateWeatherAlert(weatherDescription);
            System.out.print(alert);
        }
        if (weatherDescription == "sunny")
        {
            String alert = emailer.generateWeatherAlert(weatherDescription);
            System.out.print(alert);
        }
    }
}
