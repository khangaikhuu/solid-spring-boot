package mn.erin.systems.solid.dependency_inversion.bad;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
public class Emailer
{
    public String generateWeatherAlert(String weatherConditions)
    {
        String alert = "It is " + weatherConditions;
        return alert;
    }
}
