package mn.erin.systems.solid.dependency_inversion.good;

/**
 * @author KUvgunkhuu on 2/22/2018.
 */
interface Notifier
{
    void alertWeatherConditions(String weatherConditions);
}
